local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local  string,  type = ipairs, string, os, table, tostring, tonumber, type

local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local beautiful = require("beautiful")
local freedesktop = require("freedesktop")
local hotkeys_popup = require("awful.hotkeys_popup").widget

-- Import variables
local var = require("themes.default.variables")

-- Choose your theme of widgets here
local chosen_theme = "default"
local terminal     = var.terminal
local editor       = os.getenv("EDITOR") or "nvim"

-- {{{ Menu
local myawesomemenu = {
    {
        "hotkeys",
        function()
            return false, hotkeys_popup.show_help
        end,
    },
    { "manual",      terminal .. " -e man awesome" },
    { "edit config", terminal .. " -e nvim " .. awesome.conffile },
    { "restart",     awesome.restart },
    {
        "quit",
        function()
            awesome.quit()
        end,
    },
}

awful.util.mymainmenu = freedesktop.menu.build({
    icon_size = beautiful.menu_height or 16,
    before = {
        { "Awesome", myawesomemenu, beautiful.awesome_icon },
        -- other triads can be put here
    },
    after = {
        { "Open terminal", terminal },
        -- other triads can be put here
    },
})
