-- Defines fonts used in the diffrent widgets
local font         = {}

-- Defines fonts used in the widgets
font.first = "Droid Sans 7"
font.seperator = "FiraCode Nerd Font Mono 38"
font.seperator_alt = "Droid Sans 25"
font.linux_icon = "Droid Sans 14"
font.fs = "Droid Sans 14"
font.cpu = "Droid Sans 12"
font.temp = "Droid Sans 11"
font.mem = "JetBrainsMono Nerd Font 12"
font.update = "JetBrainsMono Nerd Font 13"
font.kernel = "JetBrainsMono Nerd Font 9"
font.vol = "JetBrainsMono Nerd Font 16"
font.taglist = "Droid Sans 21"
font.clock = "JetBrainsMono Nerd Font 13"
font.weather = "JetBrainsMono Nerd Font 13"

return font
