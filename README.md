# Awesome

**Description:** Awesome configuration
**Author:** Karl Fredin

![Awesome](./.images/awesome.png)

# Table of Content
- DEPENDENCIES
  - ARCH 
  - OPTIONAL
- INSTALLATION
- ABOUT CONFIGURATION
  - INIT/RC
  - WIDGETS
  - BINDINGS
  - THEMES
    - DEFAULTS
  - RULES

## DEPENDENCIES:
### ARCH
If you use arch you can install the dependencies with this command. If you use any other distro you will need to figure out 
the name of the packages since it is diffrent from distro to distro
```sh
# Install the packages
pacman -S awesome rofi fzf ttf-jetbrains-mono ttf-jetbrains-mono-nerd noto-fonts ttf-iosevka-nerd awesome-terminal-fonts

# Clones my repositories
git clone https://github.com/phoenix988/dotfiles.git $HOME/dotfiles
git clone https://github.com/phoenix988/dmscripts.git $HOME/dmscripts

# If you have your own scripts dont just run this if yoou dont have backups
rm -rf $HOME/.scripts
cp -r $HOME/dotfiles/.config/rofi $HOME/.config
cp -r $HOME/dotfiles/.scripts $HOME/.scripts

# Moves my dmenu scripts to correct directory
rm -rf $HOME/.dmenu
cp -r $HOME/dmscripts/.dmenu $HOME/.dmenu
```

### OPTIONAL
If you want to install my collection of fonts that can come in handy
you can clone my repo where I store a collection of fonts I use for my configs.
```sh
# Here you clone them to your user folder
git clone https://github.com/phoenix988/fonts.git $HOME/.local/share/fonts

# But you can also choose to move them to /usr/share/fonts to make it system wide
sudo cp -r $HOME/fonts/* /usr/share/fonts
```

### INSTALLATION
Finally clone my repo. Remember to back up your own awesome config if you have your own configuration
``` sh
# Create backup (optional)
cp -r $HOME/.config/awesome $HOME/.config/awesome.bak

# Remove old awesome config (dont forget to backup)
rm -rf $HOME/.config/awesome

# Clone my repository
git clone https://github.com/phoenix988/awesome.git $HOME/.config/awesome
```


# ABOUT CONFIGURATION
## INIT/RC
`rc.lua` is the main file awesome WM reads from. here I mainly configure some basic rules and etc. But I try to keep this as minimal as possible
and add the main configurations to sub files I have configured  


## WIDGETS
In the `widgets` folder I define widgets displayed in the wibar. In order to add them to the wibar you need to modify
`wibar_default.lua` under the folder `wibar` and there will be two setups functions there, one is made for the main screen and the second one will
be activated on the second screen or third screen depending on how many that you have.
There is also a `wibar_laptop.lua` file which is an alternate you can use for smaller screens 
suitable for laptops for example

## BINDINGS
Under `bindings.lua` in the root folder you define all your custom keybindings

## THEMES
Under the `themes` folder you define all the colors to be used inside the wibar. and you define custom themes here too if you want.
if you want to change the current theme you need to modify `activate_theme.lua` in the root folder. The name of the theme will be the same name as the theme folder
and the lua file needs to be named `theme.lua` and you put all the colors inside a theme table. It will be easier if you just copy one of the ready made themes
and just change the colors.

### DEFAULTS
Location `Themes/default` here you can find __default__ settings that are applied to all themes available in `themes`
`font.lua` here you define fonts and sizes for diffrent widgets in the wibar. `theme.lua` bootstraps the wibar
you do not usually need to configure this file if you want to change widget style or what widgets to display on the wibar
you configure it under `widgets`. Finally `variables.lua` is used to set general variables you want to use accross your configuration 
of awesome.



## RULES
In `rules.lua` you can add windows rules heres. That is all for this file very straight forward
